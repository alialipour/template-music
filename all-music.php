<!DOCTYPE html>
<!--[if lte IE 9]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<title> همه موزیک ها</title>
<!--=================================
Meta tags
=================================-->
<meta name="description" content="">
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=1, user-scalable=no"/>
<!--=================================
Style Sheets
=================================-->
<link href="http://fonts.googleapis.com/css?family=Lato:400,900,700,400italic,300,700italic" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,700' rel='stylesheet' type='text/css'>    
    
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/flexslider.css">
<link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="assets/css/animations.css">
<link rel="stylesheet" type="text/css" href="assets/css/dl-menu.css">
<link rel="stylesheet" type="text/css" href="assets/css/jquery.datetimepicker.css">
<link rel="stylesheet" href="assets/css/main.css">
<!--=================================
Place color files here ( right after main.css ) for example
<link rel="stylesheet" type="text/css" href="assets/css/colors/color-name.css">
===================================-->
    <link href="https://cdn.jsdelivr.net/gh/rastikerdar/vazirmatn@v33.003/Vazirmatn-font-face.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/css/style.css">
<script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    
</head>
<body>
<!--===============================
Preloading Splash Screen
===================================-->
<div class="pageLoader"></div>

<div class="majorWrap">
<!--========================================
Header Content
===========================================-->
    <?php include 'layouts/header.html';?>

    <div id="ajaxArea" class="all-music">

    <!--=================================
    Albums
    =================================-->
        <div class="mt-10 mb-50">
            <div class="container">
                <ul class="song-list text-uppercase text-bold clearfix">
                        <div class="col-xs-12 col-md-12">
                            <form class="search-widget">
                                <input type="text"  placeholder="نام آهنک یابخشی از آهنگ را واردکنید" >
                                <button><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    <div>
                        <h3 class="weblog-description all-music-title">همه موزیک ها <i class="fa fa-music"></i></h3>
                    </div>
                        <li id="singleSongPlayer-1" class="song-unit singleSongPlayer clearfix" data-before="1" title="من باهات قهرم - امیرتتلو">

                            <div id="singleSong-jplayer-4" class="singleSong-jplayer title-post" data-title=" من باهات قهرم "  data-mp3="https://dl.musicguitars.ir/Music/Amir%20Tataloo/320/Amir%20Tataloo%20-%20Man%20Bahat%20Ghahram%20%5B320%5D.mp3">
                            </div>

                            <figure><a href="music-single.php"><img src="assets/img/unnamed-4.jpg" alt="" /></a></figure>

                            <span class="playit controls jp-controls-holder">
                            <i class="jp-play pc-play"></i>
                            <i class="jp-pause pc-pause"></i>
                        </span>
                            <span class="song-title jp-title" ></span>
                            <span class="song-author title-post" data-before="خواننده:">  امیرتتلو</span>
                            <span class="song-time jp-duration title-post" ></span>
                            <form  action="https://dl.musicguitars.ir/Music/Amir%20Tataloo/320/Amir%20Tataloo%20-%20Man%20Bahat%20Ghahram%20%5B320%5D.mp3" method="get">
                                <button class="song-btn title-post  b-danger btn-download"> دانلود <i class="fa fa-download"></i></button>
                            </form>

                            <div class="audio-progress">
                                <div class="jp-seek-bar">
                                    <div class="jp-play-bar" style="width:20%;"></div>
                                </div><!--jp-seek-bar-->
                            </div><!--audio-progress-->
                        </li>
                    <li id="singleSongPlayer-2" class="song-unit singleSongPlayer clearfix" data-before="2">

                        <div id="singleSong-jplayer-5" class="singleSong-jplayer title-post" data-title="   قلب من " data-mp3="https://dl.musicguitars.ir/Music/Alireza%20Talischi/320/Alireza%20Talischi%20-%20Ghalbe%20Man%20[320].mp3">
                        </div>

                        <figure><img src="assets/img/unnamed-4.jpg" alt="" /></figure>

                        <span class="playit controls jp-controls-holder">
                            <i class="jp-play pc-play"></i>
                            <i class="jp-pause pc-pause"></i>
                        </span>
                        <span class="song-title jp-title" ></span>
                        <span class="song-author title-post" data-before="خواننده:">  علیرضا طلیسچی</span>
                        <span class="song-time jp-duration title-post" ></span>
                        <a class="song-btn title-post  b-danger " href="https://dl.musicguitars.ir/Music/Alireza%20Talischi/320/Alireza%20Talischi%20-%20Ghalbe%20Man%20[320].mp3"> دانلود <i class="fa fa-download"></i></a>


                        <div class="audio-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar" style="width:20%;"></div>
                            </div><!--jp-seek-bar-->
                        </div><!--audio-progress-->
                    </li>
                    <li id="singleSongPlayer-3" class="song-unit singleSongPlayer clearfix" data-before="2">

                        <div id="singleSong-jplayer-6" class="singleSong-jplayer title-post" data-title="    نوازش " data-mp3="https://dl.faz2music.ir/download/3471502/Amir%20Tataloo%20-%20Navazesh%20[320].mp3">
                        </div>

                        <figure><img src="assets/img/unnamed-4.jpg" alt="" /></figure>

                        <span class="playit controls jp-controls-holder">
                            <i class="jp-play pc-play"></i>
                            <i class="jp-pause pc-pause"></i>
                        </span>
                        <span class="song-title jp-title" ></span>
                        <span class="song-author title-post" data-before="خواننده:">  امیرتتلو </span>
                        <span class="song-time jp-duration title-post" ></span>
                        <a class="song-btn title-post  b-danger " href="https://dl.faz2music.ir/download/3471502/Amir%20Tataloo%20-%20Navazesh%20[320].mp3"> دانلود <i class="fa fa-download"></i></a>


                        <div class="audio-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar" style="width:20%;"></div>
                            </div><!--jp-seek-bar-->
                        </div><!--audio-progress-->
                    </li>
                    <li id="singleSongPlayer-4" class="song-unit singleSongPlayer clearfix" data-before="2">

                        <div id="singleSong-jplayer-7" class="singleSong-jplayer title-post" data-title="    دل به دل " data-mp3="https://dls.music-fa.com/tagdl/ati/Alireza%20Talischi%20-%20Del%20Be%20Del%20(320).mp3">
                        </div>

                        <figure><img src="assets/img/unnamed-4.jpg" alt="" /></figure>

                        <span class="playit controls jp-controls-holder">
                            <i class="jp-play pc-play"></i>
                            <i class="jp-pause pc-pause"></i>
                        </span>
                        <span class="song-title jp-title" ></span>
                        <span class="song-author title-post" data-before="خواننده:">  علیرضا طلیسچی </span>
                        <span class="song-time jp-duration title-post" ></span>
                        <a class="song-btn title-post  b-danger " href="https://dls.music-fa.com/tagdl/ati/Alireza%20Talischi%20-%20Del%20Be%20Del%20(320).mp3"> دانلود <i class="fa fa-download"></i></a>


                        <div class="audio-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar" style="width:20%;"></div>
                            </div><!--jp-seek-bar-->
                        </div><!--audio-progress-->
                    </li>
                    <li id="singleSongPlayer-5" class="song-unit singleSongPlayer clearfix" data-before="2">

                        <div id="singleSong-jplayer-8" class="singleSong-jplayer title-post" data-title="    دل به دل " data-mp3="https://dls.music-fa.com/tagdl/ati/Alireza%20Talischi%20-%20Del%20Be%20Del%20(320).mp3">
                        </div>

                        <figure><img src="assets/img/unnamed-4.jpg" alt="" /></figure>

                        <span class="playit controls jp-controls-holder">
                            <i class="jp-play pc-play"></i>
                            <i class="jp-pause pc-pause"></i>
                        </span>
                        <span class="song-title jp-title" ></span>
                        <span class="song-author title-post" data-before="خواننده:">  علیرضا طلیسچی </span>
                        <span class="song-time jp-duration title-post" ></span>
                        <a class="song-btn title-post  b-danger " href="https://dls.music-fa.com/tagdl/ati/Alireza%20Talischi%20-%20Del%20Be%20Del%20(320).mp3"> دانلود <i class="fa fa-download"></i></a>


                        <div class="audio-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar" style="width:20%;"></div>
                            </div><!--jp-seek-bar-->
                        </div><!--audio-progress-->
                    </li>
                    <li id="singleSongPlayer-6" class="song-unit singleSongPlayer clearfix" data-before="2">

                        <div id="singleSong-jplayer-9" class="singleSong-jplayer title-post" data-title="    دل به دل " data-mp3="https://dls.music-fa.com/tagdl/ati/Alireza%20Talischi%20-%20Del%20Be%20Del%20(320).mp3">
                        </div>

                        <figure><img src="assets/img/unnamed-4.jpg" alt="" /></figure>

                        <span class="playit controls jp-controls-holder">
                            <i class="jp-play pc-play"></i>
                            <i class="jp-pause pc-pause"></i>
                        </span>
                        <span class="song-title jp-title" ></span>
                        <span class="song-author title-post" data-before="خواننده:">  علیرضا طلیسچی </span>
                        <span class="song-time jp-duration title-post" ></span>
                        <a class="song-btn title-post  b-danger " href="https://dls.music-fa.com/tagdl/ati/Alireza%20Talischi%20-%20Del%20Be%20Del%20(320).mp3"> دانلود <i class="fa fa-download"></i></a>


                        <div class="audio-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar" style="width:20%;"></div>
                            </div><!--jp-seek-bar-->
                        </div><!--audio-progress-->
                    </li>
                    <li id="singleSongPlayer-7" class="song-unit singleSongPlayer clearfix" data-before="1">

                        <div id="singleSong-jplayer-10" class="singleSong-jplayer title-post" data-title=" من باهات قهرم " data-mp3="https://dl.musicguitars.ir/Music/Amir%20Tataloo/320/Amir%20Tataloo%20-%20Man%20Bahat%20Ghahram%20%5B320%5D.mp3">
                        </div>

                        <figure><img src="assets/img/unnamed-4.jpg" alt="" width="265" height="265"/></figure>

                        <span class="playit controls jp-controls-holder">
                            <i class="jp-play pc-play"></i>
                            <i class="jp-pause pc-pause"></i>
                        </span>
                        <span class="song-title jp-title" ></span>
                        <span class="song-author title-post" data-before="خواننده:">  امیرتتلو</span>
                        <span class="song-time jp-duration title-post" ></span>
                        <a class="song-btn title-post  b-danger " href="https://dl.musicguitars.ir/Music/Amir%20Tataloo/320/Amir%20Tataloo%20-%20Man%20Bahat%20Ghahram%20%5B320%5D.mp3"> دانلود <i class="fa fa-download"></i></a>


                        <div class="audio-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar" style="width:20%;"></div>
                            </div><!--jp-seek-bar-->
                        </div><!--audio-progress-->
                    </li>


                </ul>
            </div>

        </div>

    <!--=================================
    Similar Album Content
    =================================-->

    </div>
<!--=================================
Footer
=================================-->
    <?php  include 'layouts/footer.html';?>
</div>

<!--=================================
Script Source
=================================-->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/ajaxify.min.js"></script>
<script src="assets/js/jquery.downCount.js"></script>
<script src="assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="assets/js/jquery.bxslider.min.js"></script> <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&#038;sensor=false&#038;ver=3.0'></script>
<script src="assets/js/jplayer/jquery.jplayer.min.js"></script>
<script src="assets/js/jplayer/jplayer.playlist.min.js"></script>
<script src="assets/js/jquery.flexslider-min.js"></script>
<script src="assets/js/jquery.stellar.min.js"></script>
<script src="assets/js/jquery.sticky.js"></script>
<script src="assets/js/jquery.waitforimages.js"></script>
<script src="assets/js/masonry.pkgd.min.js"></script>
<script src="assets/js/packery.pkgd.min.js"></script>
<script src="assets/js/tweetie.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/main.js"></script>    

</body>
</html>
    